(load "../utils.lisp") ;; import with sut::function

;;; EXERCISE 1 MONTE CARLO
;; Design a Monte Carlo algorithm searching for assignments satisfying at least ⌈2m/3⌉ − 1
;; clauses of a 2-CNF, Analyze and implement it

;; Clause that satisfies at least  ⌈2m/3⌉ − 1 clauses

(defun bit-vector->assignment (bv)
  (loop for i from 0 to (1- (length bv)) collect (if (eq (elt bv i) 1) t)))

(defun random-assignment (n)
  (bit-vector->assignment (sut::random-vector n (lambda () (random 2)))))

(defun verbose-clause (clause n)
  (funcall
   clause
   (loop for i from 1 to n
	 collect 
	 (make-symbol (concatenate 'string "x" (write-to-string i))))))

(defun generate-random-2-CNF (n m)
  (let ((occ1 (sut::random-vector m (lambda () (random n))))
	(occ2 (sut::random-vector m (lambda () (random n))))
	(neg1 (sut::random-vector m (lambda () (random 2))))
	(neg2 (sut::random-vector m (lambda () (random 2)))))
    (lambda (assignment-list)
      (append '(and)
	      (loop
		for i from 0 to (1-  m)
		collect
		(list 'or
		      (if (eq (elt neg1 i) 1)
			  (list 'not (elt assignment-list (elt occ1 i)))
			  (elt assignment-list (elt occ1 i)))
		      (if (eq (elt neg2 i) 1)
			  (list 'not (elt assignment-list (elt occ2 i)))
			  (elt assignment-list (elt occ2 i)))
		      ))))))

(defun part-eval (clause)
  (mapcar #'eval (cdr clause)))

(defun count-t (lst)
  (let ((count 0))
    (dolist (item lst)
      (if item (incf count)))
    count))

(defun monte-carlo (clause n m &optional (threshold (1- (ceiling (/ (* 2 m) 3)))))
  ;; The idea is to pick an assignment at random
  ;; Then test if it succeeds in performing above the threshold
  ;; If it does, it is a witness, and the clause has the property we are looking for
  ;; If not, it still might be the case that we chose a bad assignment, but we return nil anyway
  (let* ((a (random-assignment n)))
	 ;(threshold ))
         ;(threshold (/ (* 3 m) 4)))
    (if (>= (count-t (part-eval (funcall clause a))) threshold)
	a)))

(defun las-vegas (clause n m &optional (reccounter 0))
  ;; We use the montecarlo algo.
  ;; When Monte carlo returns a witness, we know -> return the witness
  ;; Else we gamble and run the las-vegas again.
  ;; Monte carlo is > 1/2, so las vegas terminates, in expected "finite" time
  (let ((witness (monte-carlo clause n m (/ (* 3 m) 4))))  
    (if (not witness) ;; monte carlo: NO, kann man aber nicht ganz vertrauen
	(las-vegas clause n m (1+ reccounter))
	(list witness reccounter))))

(defun demo-monte-carlo (&optional (N 10) (M 10)) ; N Variables, M clauses
  (loop for trial from 1 to 1000
	counting
	(let ((claus (generate-random-2-CNF N M)))
	  (monte-carlo claus N M))))

(defun demo-las-vegas (&optional (N 10) (M 10))
  (loop for trial from 1 to 1000
	collect
	(let ((claus (generate-random-2-CNF N M)))
	  (elt (las-vegas claus N M) 1))))
