(defun translate-0-1 (item)
  (if item 1 0))

(defun count-trials (num-trial callback)
  (loop for _ from 1 to num-trial summing (funcall callback)))

;; b()
(defun biased-coin (&optional (p 0.3))
  (translate-0-1 (< (random 1.0) p)))

;; r()
(defun r (&optional (p 0.3))
  "Flip 2 coins, if equal do not use. If different use 01 as 0 and 10 as
1.
       |   0	|    1
  -----+--------+---------
       |	|
    0  |  p*p   | p(1-p)
       |	|
  -----+--------+---------
       |	|
    1  |p*(1-p)	| (1-p)(1-p)
       |	|
  -----+--------+---------
"
  (let ((a (translate-0-1 (biased-coin p)))
	(b (translate-0-1 (biased-coin p))))
    (cond ((= a b) (r))
	  ((= a 1) 1)
	  (t 0))))

;; This simulates the 'more or less' question
;; (count-trials 10000 
;; 	      (lambda () 
;; 		(translate-0-1 
;; 		 (let ((n 1001)) ; odd!
;; 		   (> (count-trials n (lambda () (biased-coin 0.5))) (floor n 2))))))


(defun factorial (n)
  (cond ((= 0 n) 1)
	(t (* n (factorial (1- n))))))

(defun binomial% (n k)
  (if (and (>= n 0) (>= k 0))
      (/ (factorial n)
	 (* (factorial k) (factorial (- n k))))))


(defun binomial%% (n k)
  (cond ((eq n k) 1)
	((eq n 0) 1)
	((eq k 0) 1)
	(t (+ (binomial%% (1- n) (1- k)) (binomial%% (1- n) k)))))
  

;(let ((n 101))
;	   (loop for i from 50 to n summing (* (binomial 101 i) (expt 0.5 n))))
