(load "../utils.lisp")

(declaim (optimize (speed 3) (safety 1)))
;; (declare dynamic-extent)

(defun bit-vector->assignment (bv)
  (loop for i from 0 to (1- (length bv)) collect (if (eq (elt bv i) 1) t)))

(defun random-assignment (n)
  (bit-vector->assignment (sut::random-vector n (lambda () (random 2)))))

(defun random-choose% (lst &optional (k 1))
  (let ((idx (sort (subseq (sut::random-shuffle (sut::arange 0 (1- (length lst)))) 0 k) '<)))
    (loop for i in idx collect (elt lst i))))

(defun random-choose (lst &optional (k 1))
  (declare (integer k))
  (let* ((idx '())
	 (ret '())
	 (len (length lst))
	 (candidate (random (1- len))))
    (loop while (not (eq (length idx) k)) do
      (when (not (member candidate idx))
	(setf idx (cons candidate idx))
	(setf ret (cons (elt lst candidate) ret)))
      (setf candidate (random (1- len))))
    ret))


(defun verbose-clause (clause n)
  (funcall
   clause
   (loop for i from 1 to n
	 collect 
	 (make-symbol (concatenate 'string "x" (write-to-string i))))))

(defun unique-list (lst)
  (cond ((null lst) lst)
	((member (car lst) (cdr lst)) (unique-list (cdr lst)))
	(t (cons (car lst) (unique-list (cdr lst))))))

(defun random-strict-2-CNF-variable-map (n m)
  (loop
    for _ from 1 to m
    collect
    (let ((ab (random-choose (sut::arange 0 (1- n)) 2)))
      (mapcar
       #'(lambda (val)
	   (if (eq 1 (random 2))
	      `(~ ,val)
	      val))
      ab
      ))))

;; if I understood correctly this is k-CNF phi_k (n,m)
(defun random-k-CNF-variable-map (k n m)
  ;; m clauses with each k literals chosen from {1,2,..,n,~1,~2,..,~n}
  (loop
    for _ from 1 to m
    collect
    (unique-list
     (sut::random-vector
      k
      (lambda ()
	(let ((a (random n)))
	  (if (eq 1 (random 2))
	      `(~ ,a)
	      a)))))))

(defun make-k-CNF (variable-map)
  (lambda (assignment-list)
    (append '(and)
	    (loop
	      for c in variable-map
	      collect
	      (append '(or)
		      (loop for v in c
			    collect
			    (if (listp v)
				(list 'not (elt assignment-list (elt v 1)))
				(elt assignment-list v))
			    ))))))

(defun eval-clauses (clause)
  (mapcar #'eval (cdr clause)))

(defun count-t (lst)
  (let ((count 0))
    (dolist (item lst)
      (if item (incf count)))
    count))

(defun count-nil (lst)
  (let ((count 0))
    (dolist (item lst)
      (if (null item) (incf count)))
    count))

(defun idx-of-t (lst)
  (remove nil (let ((i -1))
		(mapcar
		 #'(lambda (item)
		     (incf i)
		     (if item i))
		 lst))))

(defun idx-of-nil (lst)
  (remove nil (let ((i -1))
		(mapcar
		 #'(lambda (item)
		     (incf i)
		     (if (null item) i))
		 lst))))


(defun increase-every (start every)
  (let ((every-counter 0)
	(counter -1))
    (lambda ()
      (when (= (mod every-counter every) 0)
	(incf counter))
      (incf every-counter)
      (+ start counter))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; PAPADIMITRIOU
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun papadimitriou (Fmap n &optional (threshold 1))
  ;; repeate for 2n^2 times -> if we dont succed in 2n^2 tries we return nil. (Monte Carlo Algo)
  ;; a = some initial assignment
  ;; for ` = 1, ..., 2n^2 :
  ;;   if F (a) == False:
  ;;     (C, x) = (random unsatisfied clause of F, random variable in C)
  ;;     flip assignment of x in a
  ;;   else: return a
  ;; return None
  ;; the iteration is done recursively
  (labels ((internal (counter assignment)
	     (let* ((m (length Fmap))
		    (F (funcall (make-k-CNF Fmap) assignment))
		    (pF (eval-clauses F))
		    (Ftrue (count-t pF)))
	       (cond
		 ((>= (/ Ftrue m) threshold) (list assignment (- (* 2 n n) counter)))
		 ((>= counter 0)
		  (let* ((iF (idx-of-nil pF))
			 ;; C, random unsatisfied clause
			 ;(C-idx (elt (random-choose iF) 0))
			 ;(C-clause-var (elt (random-choose (elt Fmap C-idx)) 0))
			 (C-idx (random (1- (length iF))))
			 (C-clause-var
			   (elt (elt Fmap C-idx) (random (1- (length (elt Fmap C-idx))))))
			 ;; x, random variable in C
			 (var-idx (if (listp C-clause-var)
					     (elt C-clause-var 1)
					     C-clause-var)))
		    ;; flip value of x
		    (setf (elt assignment var-idx) (not (elt assignment var-idx)))
		    (internal (1- counter) assignment)))
		 (t nil)))))
    (internal (1- (* 2 n n)) (random-assignment n))))


(defun demo-papadimitriou1 (&optional (show-stuff nil) (n 10) (m 10))
  (let* ((varmap (random-strict-2-CNF-variable-map n m)))
    (when show-stuff
      (print (verbose-clause (make-k-CNF varmap) n)))
    (papadimitriou varmap n (/ 3 4))))
  ;; run time?

(defun demo-papadimitriou2 (&optional (show-stuff nil) (n 10) (m 10) (k 2))
  (let* ((varmap (random-k-CNF-variable-map k n m)))
    (when show-stuff
      (print (verbose-clause (make-k-CNF varmap) n)))
    (papadimitriou varmap n 1))) ;; NOTICE THE THRESHOLD!!

;; DIS WILL TAKE A LONG TIME!!!
(defun demo-papadimitriou-run (&optional (r 1.5))
    (loop for n from 10 to 100 by 10
	collect
	(let ((res (demo-papadimitriou2 nil n (ceiling (* r n)))))
	  (print n)
	  (if (null res) res (elt res 1)))))

;; (time (loop for _ from 1 to 10 collect
;; (demo-papadimitriou-run)))
;; Evaluation took:
;;   7239.272 seconds of real time
;;   7247.849407 seconds of total run time (7191.199765 user, 56.649642 system)
;;   [ Real times consist of 123.115 seconds GC time, and 7116.157 seconds non-GC time. ]
;;   [ Run times consist of 122.718 seconds GC time, and 7125.132 seconds non-GC time. ]
;;   100.12% CPU
;;   74,495,235 forms interpreted
;;   74,495,235 lambdas converted
;;   28,956,646,545,840 processor cycles
;;   3 page faults
;;   4,057,426,045,648 bytes consed
  
;; ((12 36  57  279 NIL NIL NIL 140 NIL NIL)
;;  (10 19  NIL NIL 121 104 141 414 NIL NIL)
;;  (19 NIL 38  NIL NIL NIL 54  NIL NIL NIL)
;;  (5  25  NIL 64  NIL 109 NIL NIL NIL NIL)
;;  (3  22  NIL 108 241 NIL NIL NIL 80  NIL)
;;  (3  NIL 15  70  NIL NIL 211 NIL NIL NIL)
;;  (16 78  45  83  208 NIL NIL NIL NIL NIL)
;;  (3  50  23  NIL 250 NIL NIL 392 NIL NIL)
;;  (3  NIL 14  237 419 70  NIL NIL NIL NIL)
;;  (1  NIL 70  NIL NIL NIL 66  NIL NIL NIL))
;;+ ----------------------------------------
;;  (10 6   7   6   5   3   4   3   1   0)

;; Vielleicht waren die CNFs aber auch garnicht lösbar!?
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; WALKER
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun walker (Fmap t-times a &optional (threshold 1))
  ;; Modified papadimitriou to only repeat t times from assignment a
  ;; for ` = 1, ..., t:
  ;;   if F (a) == False:
  ;;     (C, x) = ( some unsatisfied clause of F, random variable occuring in C)
  ;;     flip assignment of x in a
  ;;   else:
  ;;     return a
  ;; return None
  (labels ((internal (counter assignment)
	     (let* ((m (length Fmap))
		    (F (funcall (make-k-CNF Fmap) assignment))
		    (pF (eval-clauses F))
		    (Ftrue (count-t pF)))
	       (cond
		 ((>= (/ Ftrue m) threshold) (list assignment (- t-times counter)))
		 ((>= counter 0)
		  (let* ((iF (idx-of-nil pF))
			 ;; C, random unsatisfied clause
			 (C-idx (elt (random-choose iF) 0))
			 (C-clause-var (elt (random-choose (elt Fmap C-idx)) 0))
			 ;; x, random variable in C
			 (var-idx (if (listp C-clause-var)
					     (elt C-clause-var 1)
					     C-clause-var)))
		    ;; flip value of x
		    (setf (elt assignment var-idx) (not (elt assignment var-idx)))
		    (internal (1- counter) assignment)))
		 (t nil)))))
    (internal t-times a)))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; RESTARTER
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun restarter (Fmap n S)
  ;; for ` = 1, ..., S:
  ;;   a = random initial assignment
  ;;   b = WALKER(F, a, 3n)
  ;;   if b != None:
  ;;     return b
  ;; return None
  (labels ((internal-rec (counter)
	     (let* ((a (random-assignment n))
		    (b (walker Fmap (* 3 n) a)))
	       (cond ((not (null b)) b)
		     ((< counter S) (internal-rec (1- counter)))
		     (t nil)))))
    (internal-rec S)))

(defun demo-restarter(&optional (show-stuff nil) (n 10) (m 10) (k 3))
  (let* ((varmap (random-k-CNF-variable-map k n m))
	 (c (sqrt (/ 3 (* 64 pi))))
	 (valS (* (/ (sqrt n) c) (expt (/ 4 3) n) (log 2 (exp 1)))))
    (when show-stuff
      (print (verbose-clause (make-k-CNF varmap) n)))
    (restarter varmap n valS)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




(defmacro benchmarker (num-trials runs-per-trial &body body)
  `(loop
     for _ from 1 to ,num-trials
     collect
     (print (let* ((results
	     (loop
	       for _ from 1 to ,runs-per-trial
	       collect
	       (progn ,@body)))
	    (clean-results (remove nil results))
	    (fails (count-nil results)))
       (if (> (length clean-results) 0)
	   (/ (apply #'+ clean-results) (length clean-results))
	   0)))))

;; (time
;;  (let* ((r 2) (trials 100) (runs 10) 
;; 	(idx-n (increase-every 20 runs)))
;;    (benchmarker trials runs
;;      (print "..")
;;      (let* ((n (funcall idx-n))
;; 	    (m (ceiling (* r n)))
;; 	    (k 2)
;; 	    (varmap (random-k-CNF-variable-map k n m))
;; 	    (result (papadimitriou varmap n 1)))
;;        (if (not (null result))
;; 	   (elt result 1))))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; UTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun erdos-renyi-graph% (n p)
  ;; create a vector of size (* n (1- n)) random 0 and 1
  ;; indicating
  (let ((graph '())
	(counter 0)
	(edges
	  (sut::random-vector
	   (floor (* n (1- n)) 2)
	   (lambda () (if (< (random 1.0) p) 1 0)))))
    (labels ((dfs (u)
	       (loop for i from 0 to (+ )(* (1- n) u))
	       ))
      (dfs 0))))

(defun erdos-renyi-graph (n p)
  (let* ((graph '()))
    (loop
      for u from 0 to (1- n)
      do
	 (loop
	   for v from u to (1- n)
	   do
	      (if (and (< (random 1.0) p) (not (eq u v)))
		  (setf graph (cons (list u v) graph)))))
    graph))

(defun edge-list-to-dot-file (edge-list n &optional (markr nil) (markb nil))
  (with-open-file (*standard-output* "/tmp/graph_gen.txt" :direction :output :if-exists :supersede)
    (format t "graph G {~%")
    ;; fontsize=0 is an option
    (when markr
      (format t "node [ shape=point color=red];")
      (loop for i in markr do (format t " ~A" i))
      (format t ";~%"))
    (when markb
      (format t "node [ shape=point color=blue];")
      (loop for i in markb do (format t " ~A" i))
      (format t ";~%"))
    (format t "node [shape=point color=black]")
    (loop for i from 0 to (1- n)
	  do (format t "~A;~%" i))
    (loop for edge in edge-list
	  do (format t "~A -- ~A~%" (elt edge 0) (elt edge 1)))
    (format t "}~%")))

;; Why no work ?
;;  (sb-ext:run-program "/usr/bin/sfdp" '("-Tsvg" "/tmp/graph_gen.txt" ">" "/tmp/output.svg"))
;;  (sb-ext:run-program "/usr/bin/feh" '("/tmp/output.svg")))

(defun edge-list-to-dict (edge-list n)
  (let* ((D (sut::dictionary n)))
    (loop for edge in edge-list
	  do
	     (let ((u (elt edge 0))
		   (v (elt edge 1)))
	       (sut::append-to u v D)
	       (sut::append-to v u D)))
    D))

(defun random-walk-on-graph (g source target)
  (let ((n (length g))
	(history '()))
    (labels ((rw (s counter)
	       (cond ((= counter 0) nil)
		     ((= s target) 'YES)
		     (t 
		      (let* ((neighbors (cdr (assoc s g)))
			     (len  (length neighbors)))
			(if (not (member s history))
			    (setf history (cons s history)))
			(if (not (eq 0 len))
			    (rw (elt neighbors (random len)) (1- counter))
			    nil))))))
      (list (rw source (* 2 n n n)) history))))


;; dont forget to start and feh output.svg
;; while true do sfdp -Tsvg graph_gen.txt > output.svg && sleep 0.5 done

(defun demo-ustcon (&optional (show-stuff nil) (n 100) (p 0.01))
  (let* ((el (erdos-renyi-graph n p))
	 (G (edge-list-to-dict el n))
	 (a (random (1- n)))
	 (b (random (1- n)))
	 (tmp (random-walk-on-graph G a b))
	 (hist (elt tmp 1))
	 (result (elt tmp 0)))
    (when show-stuff
      (format t "a: ~A b: ~A" a b)
      (print el)
      (print G)
      (edge-list-to-dot-file el n (list a b) hist))
    result))

(defun demo-connectedness-as-n->infty (&optional (von 100) (bis 1000) (steps 100) (trials 10))
  (loop for n from von to bis by steps
	do
	   (format t "~A --------------------~%" n)
	   (let* ((r (/ (log n (exp 1)) n))
		  (pu (* r 2))
		  (pl (/ r 2))
		  (valup (/ (loop for _ from 1 to trials
				  summing 
				  (if (null (demo-ustcon nil n pu)) 0 1))
			    trials))
		  (valdown (/ (loop for _ from 1 to trials 
				    summing 
				    (if (null (demo-ustcon nil n pl)) 0 1))
			      trials)))
	     (format t " r = ~F:~%con with 2*r : ~F -> ~F~%con with r/2 : ~F -> ~F~%"
		     r pu valup pl valdown))))

;; PROMPT: (time (demo-connectedness-as-n->infty 100 500 50 20))
;; 100 --------------------
;;  r = 0.046051707:
;; con with 2*r = 0.092103414 -> 1.0
;; con with r/2 : 0.023025854 -> 0.7
;; 150 --------------------
;;  r = 0.03340424:
;; con with 2*r = 0.06680848 -> 1.0
;; con with r/2 : 0.01670212 -> 0.65
;; 200 --------------------
;;  r = 0.02649159:
;; con with 2*r = 0.05298318 -> 1.0
;; con with r/2 : 0.013245795 -> 1.0
;; 250 --------------------
;;  r = 0.022085845:
;; con with 2*r = 0.04417169 -> 1.0
;; con with r/2 : 0.011042923 -> 0.75
;; 300 --------------------
;;  r = 0.01901261:
;; con with 2*r = 0.03802522 -> 1.0
;; con with r/2 : 0.009506305 -> 0.9
;; 350 --------------------
;;  r = 0.016736953:
;; con with 2*r = 0.033473905 -> 1.0
;; con with r/2 : 0.008368476 -> 0.9
;; 400 --------------------
;;  r = 0.014978663:
;; con with 2*r = 0.029957326 -> 1.0
;; con with r/2 : 0.0074893315 -> 0.85
;; 450 --------------------
;;  r = 0.013576107:
;; con with 2*r = 0.027152214 -> 1.0
;; con with r/2 : 0.0067880535 -> 1.0
;; 500 --------------------
;;  r = 0.012429217:
;; con with 2*r = 0.024858434 -> 1.0
;; con with r/2 : 0.0062146084 -> 0.8
;; Evaluation took:
;;   751.091 seconds of real time
;;   750.265486 seconds of total run time (750.262152 user, 0.003334 system)
;;   [ Run times consist of 0.001 seconds GC time, and 750.265 seconds non-GC time. ]
;;   99.89% CPU
;;   3,004,307,364,200 processor cycles
;;   96,890,016 bytes consed
  
;; NIL
