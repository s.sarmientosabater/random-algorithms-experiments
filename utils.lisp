

(defpackage :sim-utils
  (:nicknames sut :S-UT)
  (:use :common-lisp))
(in-package :sim-utils)

(defun arange (start stop)
  "Return list from start to stop."
  (loop for i from start to stop collect i))

;; Carefull this becomes huge very quickly!!!
(defun permute-tree (lst)
  "Generate the permutation tree of a given list."
  (loop
    for i in lst
    collect
    (cons i (permute-tree (remove i lst)))))

(defun random-vector (len &optional (gen (lambda () (random 1.0))))
  "Generate a random vector of length len calling gen for every item."
  (loop
    for i from 1 to len collect (funcall gen)))

(defun random-shuffle (lst)
  "Return a list with a random permutation of lst."
  (cond ((null lst) nil)
	(t
	 (let ((cur (elt lst (random (length lst)))))
	   (cons cur (random-shuffle (remove cur lst)))))))

(defun partitions-n (callback lst n &optional partitions)
  "All posibilities to split lst into n groups."
  (if (eq (length partitions) 0)
      (setq partitions (loop for i from 1 to n collect nil)))
  (if (not (null lst))
      (loop
	for i from 0 to (1- n)
	do
	(let ((next-partitions (copy-list partitions)))
	  (cond ((null (elt next-partitions i))
		 (setf (elt next-partitions i) (list (first lst))))
		(t (push (first lst) (elt next-partitions i))))
	  (partitions-n callback (remove (first lst) lst) n next-partitions)))
      (funcall callback partitions)))

(defun n-partitions% (callback lst n)
  "All posibilities to split lst into n groups."
  (let* ((size (length lst))
	 (distribution (make-array (length lst) :initial-element 0)))
    (labels ((array2lsts ()
	       (let ((ret (loop for i from 1 to n collect '())))
		 (loop for i from 0 to (1- size) do
		   (push (elt lst i) (elt ret (elt distribution i))))
		 ret))
	     (next-distribution (lim idx)
	       (cond
		 ((eq idx size) nil)
		 ((< (1+ (svref distribution idx)) lim)
		  (incf (svref distribution idx)))
		 ((eq (1+ (svref distribution idx)) lim)
		  (setf (svref distribution idx) 0)
		  (next-distribution lim (1+ idx))))))
      (funcall callback (array2lsts))
      (loop while (next-distribution n 0) do
	(funcall callback (array2lsts))))))

(defun power-set% (callback lst)
  "Call callback on every set of the power-set of lst."
  (let* ((size (length lst))
	 (used (gensym "SENTINEL"))
	 (current-set (make-array size :fill-pointer 0))
	 (mask (make-array size :initial-contents lst)))
    (labels ((sets-dfs (counter)
	       (if (< counter size)
		   (loop for i from counter to (1- size) do
		     (unless (eq (svref mask i) used)
		       (let ((tmpval (svref mask i)))
			 ;; push nil, the item is not in the set
			 (vector-push nil current-set)
			 (setf (svref mask i) used)
			 (sets-dfs (1+ counter))
			 ;; push the value
			 (vector-pop current-set)
			 (vector-push tmpval current-set)
			 (sets-dfs (1+ counter))
			 (vector-pop current-set)
			 ;; repopulate the initial vector
			 (setf (svref mask i) tmpval))))
		   (funcall callback (remove nil current-set)))))
      (sets-dfs 0))))

(defun permutations% (callback lst)
  "All permutations. Inspiration/stolen by
https://stackoverflow.com/questions/49848994/how-to-generate-all-the-permutations-of-elements-in-a-list-one-at-a-time-in-lisp"
  (let* ((size (length lst))
	 (empty-marker (gensym "SENTINEL"))
	 (vec-lst (make-array size :initial-contents lst))
	 (permutation (make-array size :fill-pointer 0)))
    (labels ((populate (count)
	       (if (plusp count)
		 (dotimes (i size)
		   (unless (eq (svref vec-lst i) empty-marker)
		     (let ((val (svref vec-lst i)))
		       (vector-push (svref vec-lst i) permutation)
		       (setf (svref vec-lst i) empty-marker)
		       ; Important recursive call!!!
		       (populate (1- count))
     		       ; Important! Making the element available in other permutation positions.
		       (vector-pop permutation)
		       (setf (svref vec-lst i) val))))
		 (funcall callback (loop for i from 0 to (1- size) collect (elt permutation i))))))
      (populate size))))

(defun permutations-of-sets% (callback lst-of-lsts)
  "Iterates permutations of all given sets."
  (let* ((n (length lst-of-lsts))
	 (permutation (loop for i from 1 to n collect nil)))
    (labels ((perm-rec (counter )
	       (if (< counter n)
		   (permutations%
		    #'(lambda (per)
			(setf (elt permutation counter) (copy-list per))
			(perm-rec (1+ counter)))
		    (elt lst-of-lsts counter))
		   (funcall callback permutation))
	       ))
      (perm-rec 0))))

(defun power-set-partition-permutations% (callback num-partitions lst)
  (power-set%
   #'(lambda (pset)
       (n-partitions%
	#'(lambda (partition)
	    (permutations-of-sets%
	     #'(lambda (per)
		 (funcall callback per))
	     partition))
	pset num-partitions))
   lst))


;;;Stolen from https://www.lispforum.com/viewtopic.php?t=1205
(defun bit-vector->integer (bit-vector)
  "Create a positive integer from a bit-vector."
  (reduce #'(lambda (first-bit second-bit)
              (+ (* first-bit 2) second-bit))
          bit-vector))

(defun integer->bit-vector (integer)
  "Create a bit-vector from a positive integer."
  (labels ((integer->bit-list (int &optional accum)
             (cond ((> int 0)
                    (multiple-value-bind (i r) (truncate int 2)
                      (integer->bit-list i (push r accum))))
                   ((null accum) (push 0 accum))
                   (t accum))))
    (coerce (integer->bit-list integer) 'bit-vector)))


(defun factorial (n)
  (cond ((= 0 n) 1)
	(t (* n (factorial (1- n))))))



;;; DICTionary ...

(defun dictionary (size) ;; (expt 2 dim)
  (loop
    for i from 0 to (1- size)
    collect (cons i  '()))) ;; ASSOC LIST of LISTS

(defun append-to (key item dict)
  (let ((tmp (assoc key dict)))
    (rplacd tmp (append (cdr tmp) (list item)))))

(defun pop-from (key dict)
  (let* ((tmp (assoc key dict))
	 (val (cadr tmp)))
    (rplacd tmp (cddr tmp))
    val))

(defun empty-key (key dict)
  (rplacd (assoc key dict) '()))

(defun pop-all (dict)
  (remove nil (loop for i from 0 to (1- (length dict)) collect (pop-from i dict))))

(defun get-lengths (dict)
  (loop for i from 0 to (1- (length dict))
	collect (1- (length (assoc i dict)))))

(defun clear-all (dict)
  (setf dict (loop
	       for i from 0 to (1- (length dict))
	       collect (cons i  '()))))

