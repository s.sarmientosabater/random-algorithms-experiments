(load "../utils.lisp")

(defpackage :hypercube-routing
  (:use :common-lisp))
(in-package :hypercube-routing)

;;;Stolen from https://www.lispforum.com/viewtopic.php?t=1205
(defun bit-vector->integer (bit-vector)
  "Create a positive integer from a bit-vector."
  (reduce #'(lambda (first-bit second-bit)
              (+ (* first-bit 2) second-bit))
          bit-vector))

(defun integer->trunc-bit-vector (int2convert numbits)
  (labels ((internal (int)
	     (multiple-value-bind (i r) (truncate int 2)
	       (if (eq i 0)
		   (cons r nil)
		   (cons r (internal i))))))
    (make-array numbits :element-type 'bit
		    :initial-contents
		    (let* ((vals (internal int2convert))
			   (len (length vals)))
		      (reverse
		       (loop for i from 0 to (1- numbits)
			    collect
			    (if (< i len) (elt vals i) 0))
		      )))))


;(defun rv ()
;  ;; can be improved with xor
;  (make-array *d* :element-type 'bit :initial-contents (random-vector *d* (lambda () (random 2)))))

;; bit diff
(defun bit-diff-idx (source target numbits)
  (remove nil (loop
		for i from 0 to (1- numbits)
		collect (if (not (eq (elt source i) (elt target i)))
			    i))))

(defun flip-bit! (bits idx)
  (setf (aref bits idx) (mod (1+ (aref bits idx)) 2)))

(defun flip-bit (bits idx)
  (let ((a (copy-seq bits)))
    (flip-bit! a idx)
    a))
  
  
;; DETERMINISTIC STRATEGY
(defun bit-fixing-left2right (a b numbits)
  (let* ((tmp (copy-seq a))
	 (bdiffs (bit-diff-idx a b numbits))
	 (l (length bdiffs))
	 (counter 0))
    (lambda ()
      (when (and (>= l 0) (< counter l))
	(flip-bit! tmp (elt bdiffs counter))
	(incf counter)
	tmp))))

(defun bit-fixing-left2right-random-intermediate (a b numbits)
  (let* ((intermediate (integer->trunc-bit-vector (random (expt 2 numbits))))
	 (nil-switch t)
	 (fn1 (bit-fixing-left2right a intermediate numbits))
	 (fn2 (bit-fixing-left2right intermediate b numbits)))
    (print "A--")
    (print intermediate)
    (print "A--")
    (lambda ()
      (let ((tmp1 (funcall fn1)))
	(setf nil-switch (if (null tmp1)
			     (funcall fn2)
			     tmp1))))))




;; DATA structure to hold the functions
;; (defparameter *node-loads*

(defun dictionary (size) ;; (expt 2 dim)
  (loop
    for i from 0 to (1- size)
    collect (cons i  '()))) ;; ASSOC LIST of LISTS

(defun append-to (key item dict)
  (let ((tmp (assoc key dict)))
    (rplacd tmp (append (cdr tmp) (list item)))))

(defun pop-from (key dict)
  (let* ((tmp (assoc key dict))
	 (val (cadr tmp)))
    (rplacd tmp (cddr tmp))
    val))

(defun empty-key (key dict)
  (rplacd (assoc key dict) '()))

(defun pop-all (dict)
  (remove nil (loop for i from 0 to (1- (length dict)) collect (pop-from i dict))))

(defun get-lengths (dict)
  (loop for i from 0 to (1- (length dict))
	collect (1- (length (assoc i dict)))))

(defun clear-all (dict)
  (setf dict (loop
	       for i from 0 to (1- (length dict))
	       collect (cons i  '()))))


;;; SIMULATE
(defun init-data (dim sources targets strategy)
  (let ((dict (dictionary (expt 2 dim))))
    (loop
      for i from 0 to (1- (length dict))
      do
	 (append-to i (funcall strategy (elt sources i) (elt targets i) dim) dict))
    dict))

(defun create-cube (dim strategy)
  (declare (type fixnum dim))
  (let* ((sorted  (sut::arange 0 (expt 2 dim)))
	 (perm (sut::random-shuffle sorted))
	 (sources (mapcar
		   #'(lambda (i) (integer->trunc-bit-vector i dim)) sorted))
	 (targets (mapcar
		   #'(lambda (i) (integer->trunc-bit-vector i dim)) perm)))
	 (init-data dim sources targets strategy)))


(defun time-step (dict)
  (let ((fns (pop-all dict)))
    (loop for fn in fns
	  do
	     (let ((tmp (funcall fn)))
	       (when (not (null tmp))
		   (append-to (bit-vector->integer tmp) fn dict))))))


;;;;

(defun simulate (cube  &optional (output nil))
  (loop while (< 0 (apply #'+ (get-lengths cube)))
	do
	   (time-step cube)
	   (if output (print (get-lengths cube)))))

(declaim (optimize (speed 3) (debug 0) (safety 0)))

(sb-profile:profile simulate time-step get-lengths bit-fixing-walk)

;; (let* ((a (integer->trunc-bit-vector 34 10))
;; 			  (b (integer->trunc-bit-vector 12 10))
;; 			  (fn (bit-fixing-left2right-random-intermediate a b 10))
;; 			  (trig t))
;; 		     (print a)
;; 		     (print b)
;; 		     (print "-====")
;; 		     (loop while trig do
;; 		       (print (setf trig (funcall fn)))))
